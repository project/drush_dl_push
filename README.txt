drush dl push
=============

This simple script will automatically detect when you "drush dl" a project into
a folder that is in a git repository, then asks if you want to commit and push
it.

Usage
=====
To use, simply answer "yes" when you download a module:

  $ drush @mysite dl captcha
  
  Project captcha (7.x-1.0-beta2) downloaded to /path/to/mysite.com/sites/all/modules/contrib/captcha. 
  Git repository found in /path/to/mysite.com/
  # On branch master
  # Untracked files:
  #   (use "git add <file>..." to include in what will be committed)
  #
  #	sites/all/modules/contrib/captcha/
  nothing added to commit but untracked files present (use "git add" to track)
  
  Add to git repo and push? (y/n): y
  
  Pushed!
  To git@github.com:devudo/devudo.com.git 
     a57e5c4..ca96f1c  master -> master
  Project captcha contains 2 modules: image_captcha, captcha.


Installation
============

This being a drush module, you need drush.  If you don't have drush, go get it
NOW at http://drupal.org/project/drush.

  $ drush dl drush_dl_push
 
That's it!  Have fun!