<?php
/**
 * @file dl.push.drush
 * 
 * Offers to commit and push the code you just drush dl'd.
 */

/**
 * Implements hook_drush_pm_post_download()
 *
 * Offers to git add and push the recently downloaded project.
 */
function dl_push_drush_pm_post_download($project, $release) {
  $path_to_project = $project['full_project_path'];

  // Find the toplevel directory for this git repository
  // If shell returns False, this is not a git repo
  if (!drush_shell_cd_and_exec($path_to_project, 'git rev-parse --show-toplevel')){
    return drush_set_error('DRUSH_DOWNLOAD_PUSH_FAILED', dt("Git repository not found in: !path", array('!path' => $path_to_project)));
  } else {
    $path_to_repo = implode("\n", drush_shell_exec_output());     
    drush_shell_cd_and_exec($path_to_repo, 'git status');
    $status = implode("\n", drush_shell_exec_output());
  }
    
  drush_log(dt("Git repository found in !path", array('!path' => $path_to_repo)), 'ok');
  drush_log($status, 'ok');
  
  if (drush_confirm(dt('Add to git repo and push?'))){
      //Generate commit message
     $message = array();
     $message[] = "Project Added: " . $release['name'];
     $message[] = str_repeat('-', strlen($message[0]));
     $message[] = "This commit was triggered by drush dl " . $project['name'];
     $message[] = str_repeat('-', strlen($message[0]));
     if (drush_get_option('message')){
       $message[] = drush_get_option('message');
     }
     $message = implode("\n", $message);
 
     $data = array(
       'message' => $message,
     );
     
     // Add
     if (!drush_shell_cd_and_exec($path_to_project, 'git add .')){
      $output = drush_shell_exec_output();
      drush_set_error('DRUSH_DOWNLOAD_PUSH_FAILED', dt('git add . failed in !path', array('!path' => $path_to_project)));
       return drush_set_error('DRUSH_DOWNLOAD_PUSH_FAILED', $output);
     }
     
     // Commit
     $message = escapeshellarg($message);
      if (!drush_shell_cd_and_exec($path_to_project, "git commit -m $message")){
        $output = drush_shell_exec_output();

        drush_set_error('DRUSH_DOWNLOAD_PUSH_FAILED', dt('git commit failed!'));
        return drush_set_error('DRUSH_DOWNLOAD_PUSH_FAILED', $output);
     }
     
     // Push
     if (!drush_shell_cd_and_exec($path_to_project, "git push")){
      $output = drush_shell_exec_output();
       drush_set_error('DRUSH_DOWNLOAD_PUSH_FAILED', dt('git push failed!'));
       return drush_set_error('DRUSH_DOWNLOAD_PUSH_FAILED', $output);
     }
     
     drush_log(dt('Pushed!'), 'ok');
     $output = implode("\n", drush_shell_exec_output());
     drush_log($output, 'ok');
     
  }
}